#bad dockerfile
FROM alpine:latest
EXPOSE 22
RUN apk add curl
CMD /bin/sh


# good dockerfile
#FROM alpine:latest
#USER 65534:65534
#EXPOSE 9999
#HEALTHCHECK CMD /bin/date || exit 1
#CMD /bin/sh
